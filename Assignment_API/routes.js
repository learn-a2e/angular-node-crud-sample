const express = require('express');
const routes = express.Router();
const userroute = require('./controllers/user.controller');

routes.use('/user', userroute);

module.exports = routes;