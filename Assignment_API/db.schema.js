const mongoose = require('mongoose');
const schema = mongoose.Schema;


const userSchema = new schema({
    userItemList: [{
        userItemID: { type: String },
        userItemName: { type: String },
        userItemAddress: { type: String }
    }]
});

const singleUserSchema = new schema({
    primary:{ type: String },
    userID: { type: String, required: true },
    userName: { type: String, required: true },
    userAddress: { type: String, required: true }
});

mongoose.model('user', userSchema, 'userlist');
mongoose.model('user1', singleUserSchema, 'userlist1');

mongoose.connect('mongodb://localhost:27017/assignment',(err)=>{
    if(err){
        console.log(err);
        process.exit(-1);
    }
});

module.exports = mongoose;
