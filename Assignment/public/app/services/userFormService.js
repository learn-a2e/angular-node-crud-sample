angular.module('userFormService', [])

    .factory('userForm', ['$http', function ($http) {
        var UserFormFactory = [];

        //save user
        UserFormFactory.insertUserData = function (data) {
            return $http.post('http://localhost:8701/user/addSingleUser',data).then(function (data2) {
                return data2;
            })
        };

          //save user as a bulk
          UserFormFactory.insertAllUserData = function (data) {
            return $http.post('http://localhost:8701/user/add',data).then(function (data2) {
                return data2;
            })
        };

        return UserFormFactory;
    }]);